const crypto = require('crypto');

exports.avatarOptionsHashify = function (srcString) {
const hash = crypto.createHash('sha256');
    hash.update(srcString);
    const hashString = hash.digest('hex');
    var param_array = hashString.match(/.{1,5}/g).map((item)=>{
        return parseInt(item, 16)
    });
    console.log(param_array);

    return {
        skin_color: skin_colors[param_array[0] % skin_colors.length],
        beard_index: (param_array[1] % 4) + 1,
        beard_color: hair_colors[param_array[2] % hair_colors.length],
        mouth_index: (param_array[3] % 2) + 1,
        mouth_color: (param_array[3] % 2)?"fff":"222",
        hair_index: (param_array[5] % 9) + 1,
        hair_color: hair_colors[param_array[6] % hair_colors.length],
        body_index: (param_array[7] % 3) + 1,
        body_color: fade_colors[param_array[8] % fade_colors.length],
        body_accent_1: fade_colors[param_array[9] % fade_colors.length],
        body_accent_2: fade_colors[param_array[10] % fade_colors.length],
    }
}


const skin_colors = [
    'f9dac5',
    'c68458',
    'e5b295',
    'f9d5b4',
    'f7d9b5',
    'd8aa8b',
]

const hair_colors = [
    '904f22',
    '3a1e0b',
    'f7d060',
    'f94831',
    '983529',
    '665f72',
    '423a40',
    '534741',
    '333333',
]

const fade_colors = [
    'D8334A',
    'ED5565',
    'FC6E51',
    'FFCE54',
    'E8CE4D',
    'A0D468',
    '48CFAD',
    'A0CECB',
    '4FC1E9',
    '5D9CEC',
    '8067B7',
    'AC92EC',
    'EC87C0',
    'F5F7FA',
    'CCD1D9',
    '656D78',
    '3C3B3D',
]
