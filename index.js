const express = require('express'),
	app = express(),
	bodyParser = require('body-parser'),
	puppeteer = require('puppeteer'),
	compression = require('compression'),
	expressNunjucks = require('express-nunjucks'),
	nunjucks = require('nunjucks'),
	path = require('path'),
	{ check, validationResult } = require('express-validator/check'),
	{ matchedData, sanitize } = require('express-validator/filter')
	
const {avatarOptionsHashify} = require('./lib/avatar')

app.use(compression());
app.use(bodyParser.urlencoded({extended: false, limit: '50mb'}));
app.use(bodyParser.json({limit: '50mb'}));

const njk = expressNunjucks(app, {
    noCache: true
});

app.set('views', './templates/assets')

var port = process.env.PORT || 8083;

app.get('/', (req, res) => {
	res.json({message: 'Hello'});
});

app.get('/avatar', [
	check('email')
	.isEmail()
	.withMessage('must be an email')
	.trim()
	.normalizeEmail()
], (req, res, next) => {
	const errors = validationResult(req);
	if (!errors.isEmpty()) {
	  return res.status(422).json({ errors: errors.mapped() });
	}

	const params = matchedData(req);
	
	templateData = avatarOptionsHashify(params.email)
	
	const puppeteer = require('puppeteer');
	(async() => {
		const browser = await puppeteer.launch({args: ['--no-sandbox', '--disable-setuid-sandbox']});
		const page = await browser.newPage();
		await page.setViewport({
			width:512,
			height:512
		});

		var tmplRender = await nunjucks.render(path.resolve(__dirname, './templates/assets/base.html'), templateData);
		await page.goto('data:text/html,' + tmplRender, {waitUntil: 'load', networkIdleTimeout: 500});
		
		var screenShot = await page.screenshot({
			clip: {
				x: 0,
				y:0,
				width:512,
				height:512
			},
			omitBackground: true
		});
		browser.close();

		res.setHeader('Content-type', 'image/png');
		res.send(screenShot);
		
		
	})();
})

app.listen(port);
console.log('running avatar on ' + port);

	